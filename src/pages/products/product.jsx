import {Component} from "react";
import PropTypes from 'prop-types';
import Button from "../../UI/Button/button";
import Modal from "../../UI/Modal/modal";
import star from "../../image/starWhite.png"
import starColor from "../../image/starColor1.png"
import s from "./product.module.scss"



class Product extends Component {
    constructor(props) {
        super(props);
        this.name = props.name;
        this.price = props.price;
        this.url = props.url;
        this.number = props.number;
        this.volume = props.volume;
        this.counterCartPlus = props.counterCartPlus;
        this.counterCartMinus = props.counterCartMinus;
        this.counterStarPlus = props.counterStarPlus;
        this.counterStarMinus = props.counterStarMinus;
    }

    state = {
        onStar: false,
        onCart: false,
        openModal: false,
        header: '',
        closeButton: false,
        text: '',
        actions: <></>,
    }

    handleClickStar = () => {

        if (!!this.state.onStar) {
            this.setState({onStar: false});
            this.counterStarMinus();
            localStorage.removeItem(`onStar${this.props.number}`)
        } else {
            this.setState({onStar: true});
            this.counterStarPlus();
            localStorage.setItem(`onStar${this.props.number}`, 'true')
        }
    }

    handleClickCart = () => {

        if (!!this.state.onCart) {
            this.setState({onCart: false});
            this.counterCartMinus();
            localStorage.removeItem(`onCart${this.props.number}`)
        } else {
            this.setState({onCart: true});
            this.counterCartPlus();
            localStorage.setItem(`onCart${this.props.number}`, 'true')
        }
        this.closeModal()
    }

    showModalFirst = () => {
        this.setState({
            openModal: true,
            header: "Добавити обраний товар до кошика?",
            text: "Міністерство охорони здоров'я попереджає: помірне споживання алкоголю корисне для Вашого здоров'я!",
            closeButton: true,
            actions: <>
                <Button text={'Ok'}
                        backgroundColor={'#5dc259'}
                        onClick={this.handleClickCart}>
                </Button>
                <Button text={'Cancel'}
                        backgroundColor={'#dc6161'}
                        onClick={this.closeModal}>
                </Button>
            </>,
        })
    }

    closeModal = () => {
        this.setState({
            openModal: false,
        })
    }

    componentDidMount() {
        if (!!localStorage.getItem(`onStar${this.props.number}`)) {
            this.setState({onStar: true})
        }
        if (!!localStorage.getItem(`onCart${this.props.number}`)) {
            this.setState({onCart: true})
        }
    }

    render() {
        return (
            <>
                <div className={s.product}>
                    <img className={s.image} src={this.props.url} alt={'productFoto'}/>
                    <p className={s.name}>{this.props.name}</p>
                    <p className={s.price}>{this.props.price}</p>
                    <div className={s.wrappVolume}>
                        <p className={s.number}>Арт.{this.props.number}</p>
                        <p className={s.volume}>Об'єм: {this.props.volume}</p>
                    </div>
                    <div className={s.buttonWrapper}>
                        {!this.state.onCart && <Button onClick={this.showModalFirst}
                                                       text={'ADD TO CART'}
                                                       backgroundColor={'#94ef96'}>
                        </Button>}
                        {this.state.onCart && <Button onClick={this.handleClickCart}
                                                      text={'REMOVE FROM CART'}
                                                      backgroundColor={'#2ac72a'}>
                        </Button>}
                        <button className={s.buttonStar} onClick={this.handleClickStar}>
                            <img className={s.Star} src={this.state.onStar ? starColor : star} alt="star"/>
                        </button>
                    </div>
                </div>
                <Modal openModal={this.state.openModal}
                       header={this.state.header}
                       text={this.state.text}
                       actions={this.state.actions}
                       onClose={this.closeModal}
                       closeButton={this.state.closeButton}>
                </Modal>
            </>
        )
    }
}

Product.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    url: PropTypes.string,
    number: PropTypes.string,
    volume: PropTypes.string,
    counterCartPlus: PropTypes.func,
    counterCartMinus: PropTypes.func,
    counterStarPlusPlus: PropTypes.func,
    counterStarMinusMinus: PropTypes.func,
}

Product.defaultProps = {
    name: 'Drink',
    price: '000.00',
    number: '1111',
    volume: '0.0 l',
}

export default Product