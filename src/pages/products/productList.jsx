import {Component} from "react";
import {request} from "../../tools/request";
import Product from "./product";
import s from "./productList.module.scss";
import shoppingCart from "../../image/shoppingCart.png";
import starColor from "../../image/starColor1.png"


class ProductList extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        products: [],
        counterCart: 0,
        counterStar: 0,
    }

    async componentDidMount() {
        const drinksList = await request();
        this.setState({
            products: drinksList
        })
        this.setState({counterStar: Number (localStorage.getItem('counterStar'))});
        this.setState({counterCart: Number (localStorage.getItem('counterCart'))});
    }

    componentDidUpdate() {
        localStorage.setItem('counterCart', this.state.counterCart)
        localStorage.setItem('counterStar', this.state.counterStar)
    }

    paintProductCard(product) {
        return (<Product
            key={product.number}
            name={product.name}
            price={product.price}
            url={product.url}
            number={product.number}
            volume={product.volume}
            counterCartPlus={this.counterCartPlus}
            counterCartMinus={this.counterCartMinus}
            counterStarPlus={this.counterStarPlus}
            counterStarMinus={this.counterStarMinus}>
        </Product>)
    }

    counterCartPlus = () => {
        this.setState({counterCart: this.state.counterCart + 1 });
        localStorage.setItem('counterCart', this.state.counterCart)
    }

    counterCartMinus = () => {
        this.setState({counterCart: this.state.counterCart - 1 });
        localStorage.setItem('counterCart', this.state.counterCart)
    }

    counterStarPlus = () => {
        this.setState({counterStar: this.state.counterStar + 1 });
        localStorage.setItem('counterStar', this.state.counterStar)
    }

    counterStarMinus = () => {
        this.setState({counterStar: this.state.counterStar - 1 });
        localStorage.setItem('counterStar', this.state.counterStar)
    }

    render() {
        return (
            <>
                <div className={s.header}>
                    <p className={s.title}>ALCO-MARKET</p>
                    <div className={s.headerWrapp}>
                        <div className={s.headerStarWrapp}>
                            <img className={s.shoppingStar} src={starColor}/>
                            {this.state.counterStar > 0 && <span className={s.spanStarCounter}>{this.state.counterStar}</span>}
                        </div>
                        <div className={s.headerCartWrapp}>
                            <img className={s.shoppingCart} src={shoppingCart}/>
                            {this.state.counterCart > 0 && <span className={s.spanCardCounter}>{this.state.counterCart}</span>}
                        </div>
                    </div>
                </div>
                <div className={s.productList}>
                    {this.state.products.length > 0 && this.state.products.map(product => this.paintProductCard(product))}
                </div>
            </>
        )
    }
}


export default ProductList







