import {Component} from "react";
import ProductList from "./pages/products/productList"
import s from './App.module.scss'

class App extends Component {

    render() {
        return (
            <div className={s.app}>
                <ProductList></ProductList>
            </div>
        )
    }
}


export default App;
