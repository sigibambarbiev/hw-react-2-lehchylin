import {Component} from "react";
import PropTypes from 'prop-types';
import s from './Button.module.scss';


class Button extends Component {
    constructor(props) {
        super(props);
        this.onClick = props.onClick;
        this.text = props.text;
        this.backgroundColor = props.backgroundColor;
    }

    render() {
        return (
            <button className={s.btn} onClick={this.props.onClick} style={{backgroundColor: this.backgroundColor}}>
                {this.text}
            </button>
        )
    }
}

Button.propTypes = {
    onClick: PropTypes.func,
    text: PropTypes.string,
    backgroundColor: PropTypes.string
}

Button.defaultProps = {
    text: 'Button text',
    backgroundColor: '#FFFF'
}

export default Button
